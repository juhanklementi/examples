package com.example.myapplication

import android.app.Application
import android.os.Handler
import android.os.StrictMode

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        // Comment out this if you need to see that network on Main thread fails without this
        Handler().postAtFrontOfQueue { StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().permitAll().build()) }
    }
}