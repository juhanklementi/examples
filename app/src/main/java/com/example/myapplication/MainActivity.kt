package com.example.myapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import org.json.JSONObject
import java.io.BufferedReader
import java.net.HttpURLConnection
import java.net.URL


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var text = ""
        try {
            val con : HttpURLConnection = URL("http://worldclockapi.com/api/json/utc/now").openConnection() as HttpURLConnection
            val inputStream = con.inputStream.buffered()

            val content = StringBuilder()
            val reader = BufferedReader(inputStream.reader())
            reader.use { reader ->
                var line = reader.readLine()
                while (line != null) {
                    content.append(line)
                    line = reader.readLine()
                }
                text = JSONObject(content.toString()).getString("currentDateTime")
            }
        } catch (e : Exception) {
            Log.e("MainActivity","Failed ", e)
            text = "Failed to get data from network: $e"
        }
        findViewById<TextView>(R.id.text).text = text
    }
}
